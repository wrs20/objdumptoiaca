Tool to add start and end markers to bytes from binary or objdump output for use in the intel iaca tool. 

Requires Python 3.

bytes to iaca
=============

Extract bytes within an interval of a binary file and add iaca start and end markers.

Start and end bytes must be given:
```
    -s      Byte to start extraction from.
    -e      Byte to end extraction at.
```
Input and output files are optional if not passed stdin and stdout are used:
```
    -i      Input binary.
    -o      File to write bytes to suitable for using as an iaca input.
```
Examples:
    
(1)
```
bytestoiaca -s 690 -e 69b -i test.so -o test.iaca
iaca -64 test.iaca
```
(2)

```
cat test.so | bytestoiaca -s 690 -e 69b -o test.iaca
iaca -64 test.iaca
```
(3)

```
cat test.so | bytestoiaca -s 690 -e 69b > test.iaca

```

objdump to iaca
===============

Usage: objdumptoiaca <options>
Extract bytes within an interval of output from "objdump -d <file>" and add iaca start and end markers.

Start and end bytes must be given:
```
    -s      Byte to start extraction from.
    -e      Byte of objdump line to end extraction at, this line is included.
```
Input and output files are optional if not passed stdin and stdout are used:
```
    -i      Input file containing "objdump -d <file>" output.
    -o      File to write bytes to suitable for using as an iaca input.
```
Examples:
    
(1) Input and outfile specified
```
objdump -d test.so > test.s
objdumptoiaca -s 690 -e 69b -i test.s -o test.iaca
iaca -64 test.iaca
```
(2) Input from stdin and outout to file.
```
objdump -d test.so | objdumptoiaca -s 690 -e 69b -o test.iaca
iaca -64 test.iaca
```
(3) Input from stdin and output to stdout.
```
objdump -d test.so | objdumptoiaca -s 690 -e 69b > test.iaca
```

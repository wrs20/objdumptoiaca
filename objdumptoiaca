#!/usr/bin/env python3

__author__ = "W.R.Saunders"
__copyright__ = "Copyright 2018, W.R.Saunders"
__license__ = "GPL"

import sys
import re
import os
import getopt

MARKER_START = bytearray(b'\x0f\x0b\xbb\x6f\x00\x00\x00\x64\x67\x90')
MARKER_END = bytearray(b'\xbb\xde\x00\x00\x00\x64\x67\x90\x0f\x0b')

def _bytes_from_line(line, start, end):
    ba = []
    if re.match("^[ ]*[0-9a-f]*:.*$",line):
        # split the byte marker from the instructions
        line_split = line.split(":")
        # get the start byte of the line
        line_byte = int(line_split[0].lstrip(), 16)
        # get the opcodes and human parsable part
        line_op =line_split[1].lstrip()

        # split the opcodes from asm
        line_asm = line_op.split("\t")

        # find all bytes in the line
        op = re.findall("[0-9a-f][0-9a-f]", line_asm[0])

        ops = ''
        for i in op:
            ops += i
        if (start <= line_byte <= end):
            ba.append(bytearray.fromhex(ops))
    return ba


def objdump_to_iaca(filename_in, filename_out, start, end):
    ba = []

    if filename_out is sys.stdout.buffer:
        outfh = sys.stdout.buffer
    else:
        outfh = open(filename_out, 'wb')
    
    if filename_in is sys.stdin:
        infh = sys.stdin
    else:
        infh = open(filename_in)
    
    outfh.write(MARKER_START)
    for line in infh:
        ba += _bytes_from_line(line, start, end)

    for opx in ba:
        outfh.write(opx)
    outfh.write(MARKER_END)
    
    if filename_out is not sys.stdout.buffer:
        outfh.close()
    if filename_in is not sys.stdin:
        infh.close()


def _print_help():
    print("""Usage: objdumptoiaca <options>
Extract bytes within an interval of output from "objdump -d <file>" and add 
iaca start and end markers.

Start and end bytes must be given:
    -s      Byte to start extraction from.
    -e      Byte of objdump line to end extraction at, this line is included.

Input and output files are optional if not passed stdin and stdout are used:
    -i      Input file containing "objdump -d <file>" output.
    -o      File to write bytes to suitable for using as an iaca input.

Examples:
    
(1)
    objdump -d test.so > test.s
    objdumptoiaca -s 690 -e 69b -i test.s -o test.iaca
    iaca -64 test.iaca

(2)
    objdump -d test.so | objdumptoiaca -s 690 -e 69b -o test.iaca
    iaca -64 test.iaca

(3)
    objdump -d test.so | objdumptoiaca -s 690 -e 69b > test.iaca

""")

def _bad_option():
    print("Mandatory option missing or bad argument passed,",
            "read help with '-h' option.")
    quit()

if __name__ == '__main__':

    filename_in = sys.stdin
    filename_out = sys.stdout.buffer
    start=None
    end=None

    opts, args = getopt.getopt(sys.argv[1:], "i:o:s:e:h", ())
    try:
        for o, a in opts:
            if "-h" == o:
                _print_help()
                quit()
            elif "-i" == o:
                filename_in = str(a)
            elif "-o" == o:
                filename_out = str(a)
            elif "-s" == o:
                start = int(a, 16)
            elif "-e" == o:
                end = int(a, 16)
            else:
                _bad_option()
    except Exception as e:
        _bad_option()

    if start is None:
        _bad_option()
    if end is None:
        _bad_option()
    
    objdump_to_iaca(filename_in, filename_out, start, end)


